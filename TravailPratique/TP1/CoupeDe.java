

/**
 * Decrit une coupe.
 * Cette coupe indique une nouvelle position de la case zero mais
 * ne modifie pas ou le tableau termine.
 *
 * @see CoupeA
 * @see Coupe
 * @see CoupeDeA
 * @see TableauPartiel */
public class CoupeDe extends Coupe {
    protected int debut;

    /**
     * Construit une nouvelle coupe.
     *
     * Cette coupe commence a l'indice <code>debut</code> et termine
     * a la fin du tableau.
     * @param debut l'indice de la premiere case du tableau (inclusive).
     *              Cette valeur doit etre plus grande que 0 et
     *              plus petite que la taille du tableau.
     */
    public CoupeDe( int debut ){
        this.debut = debut;
    }

    /**
     * Retourne l'indice de debut du tableau.
     * @return l'indice de la premiere case relative du tableau.
     */
    public int getDebut(){
        return debut;
    }
}
