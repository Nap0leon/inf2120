/**
 * Travail Pratique dans le cadre du cours Programmation 2 - INF2120.
 * Consctruction de coupes de Tableau à l'aide d'une hierarchie de classe.
 *
 * @author (Maxime Masson)
 * @codepermanent (MASM06079507)
 * @author (Jan Villapaz)
 * @codepermanent (VILJ3057940)
 */
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

/**
 * Classe decrivant un tableau pour lequel il est possible de
 * modifier le mode d'acces relatif.
 * Les classes <code>Coupe</code>, <code>CoupeA</code>, <code>CoupeDe</code> et <code>CoupeDeA</code>
 * vont etre utilise pour decrire les nouvelles bornes relatives.
 *
 * @param <E> Le type des elements du tableau
 *
 * @see Coupe
 * @see CoupeA
 * @see CoupeDe
 * @see CoupeDeA
 */
public class TableauPartiel<E> {
    /**
     * Une reference vers le tableau qui contiendra les valeurs.
     * Les commentaires des methodes indique quand seulement la
     * reference vers ce tableau doit etre copie et quand le tableau
     * doit etre completement copie.
     */
    protected ArrayList<E> tableau;
    protected int debut;
    protected int fin;


    /**
     * Constructeur.
     *
     * @param tableau Un tableau Java qui contient les valeurs qui seront insere
     *                dans le <code>TableauPartiel</code> nouvellement construit.
     */
    public TableauPartiel( E[] tableau ) {
        this.tableau = new ArrayList<E>(Arrays.asList(tableau));
        this.debut = 0;
        this.fin = tableau.length - 1;

    }

    /**
     * Constructeur.
     *
     * Ce constructeur fait un <code>TableauPartiel</code> qui contiendra une copie
     * des elements du tableau en argument.  Les deux instances seront donc
     * independante.
     *
     * @param tableauPartiel Le <code>TableauPartiel</code> a copier.
     */
    public TableauPartiel( TableauPartiel<E> tableauPartiel ) {
        this.tableau = new ArrayList<E>();
        for (int i = tableauPartiel.debut ; i  < tableauPartiel.tableau.size(); i++) {
            this.tableau.add(tableauPartiel.tableau.get(i));
        }
        this.debut = 0;
        this.fin = this.tableau.size() - 1;
    }

    /**
     * Consctructeur.
     * 
     * Ce constructeur crée une coupe de <code>tableauPartiel</code> par
     * reference qui contient les valeurs situe entre les index <code>debut</code> 
     * et <code>fin</code>. 
     * 
     * @param tableauPartiel le <code>TableauPartiel</code> à couper
     * @param debut determine l'index 0 de la coupe
     * @param fin determine le dernier index de la coupe
     */
    private TableauPartiel(TableauPartiel<E> tableauPartiel, int debut, int fin) {
        this.tableau = tableauPartiel.tableau;
        this.debut = debut;
        this.fin = fin;
    }

    /**
     * Verifie si un element est present dans le tableau.
     *
     * @param element L'element recherche dans le tableau.
     * @return <code>true</code> si l'element est dans le tableau,
     *         <code>false</code> sinon.  La methode <code>equals</code> est
     *         utilise pour rechercher l'element.
     */
    public boolean contient( E element ) {
        int i = this.debut;
        while ( i < this.fin && (!element.equals(this.tableau.get(i)))) {
            i++;
        }
        if ( i < this.fin) {
            return true;
        } else return false;
    }

    /**
     * Retourne l'element du tableau a la case indique par la <code>position</code>.
     *
     * @param position L'indice de l'element a retourner
     * @return Une reference vers l'element dans la case indique par la
     *         <code>position</code> dans le tableau.
     * @throws IndexOutOfBoundsException Lance si la <code>position</code>
     *         indiquee n'est pas dans le tableau.
     */
    public E get( int position ) throws IndexOutOfBoundsException {
        if (position > this.taille() - 1) {
            throw new IndexOutOfBoundsException();
        } else {
            return this.tableau.get(this.debut + position);
        }
            
    }
    /**
     * Indique si le tableau est vide.
     *
     * @return <code>true</code> si le tableau a une taille de 0, <code>false</code>
     *         sinon.
     */
    public boolean estVide(){
        if (this.taille() == 0) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Place une nouvelle valeur a la <code>position</code> indique.
     *
     * @param position L'indice de l'element a modifier.
     * @param element L'element qui remplace l'ancien.
     * @throws IndexOutOfBoundsException Lance si la <code>position</code>
     *         indiquee n'est pas dans le tableau.
     */
    public void set( int position, E element ) throws IndexOutOfBoundsException {
        if (position <this.debut || position >this.fin - 1) {
            throw new IndexOutOfBoundsException();
        } else {
            this.tableau.set(position + this.debut, element);
        }
        
    }

    /**
     * Le nombre d'element que contient le tableau.
     *
     * @return Le nombre d'element que contient le tableau.
     */
    public int taille() {
        if (this.fin <= this.debut) {
            return 0;
        } else {
            return this.fin - this.debut + 1;
        }
    }

    /**
     * Construit une nouvelle perspective sur le tableau.
     *
     * Un nouveau <code>TableauPartiel</code> est contruit en utilisant
     * une coupe pour decrire la nouvelle perspective du tableau.
     * Les cases modifiees dans le nouveau tableau seront aussi modifiees
     * dans l'ancien, et vice versa.
     *
     * @param coupe La description de la coupe a faire sur le tableau.
     * @return Un nouveau <code>TableauPartiel</code> decrit par la coupe.
     *         Ce nouveau tableau contient une reference sur le tableau
     *         original.
     * @throws IndexOutOfBoundsException lance si la coupe contient des indices
     *         invalide.
     */
    public TableauPartiel<E> coupe( Coupe coupe ) throws IndexOutOfBoundsException {
        if ( coupe.getDebut() < this.debut || this.fin < coupe.getFin()) {
            throw new IndexOutOfBoundsException();
        } else {
            int debut = coupe.getDebut();
            int fin = coupe.getFin() - 1;
            if (coupe.getFin() == -1) {
                fin = this.tableau.size() - 1;
            } 
            return new TableauPartiel(this, debut, fin);
        }

    }

    /**
     * Trouve la position d'un element dans le tableau.
     *
     * @param element L'element cherche dans le tableau.
     * @return La position de la premiere occurence de l'element.
     *         La methode <code>equals</code> est utilise pour
     *         rechercher l'element.
     * @throws ElementNonPresentException Lancee si l'element n'est pas
     *         dans le tableau.
     */
    public int position( E element ) throws ElementNonPresentException {
        int i = this.debut;
        while ( i < this.fin && (! this.tableau.get(i).equals(element))) {
            i++;
        }
        if (i < this.fin) {
            return this.tableau.indexOf(element);
        } else {
            throw new ElementNonPresentException();
        }
    }

    /**
     * NON EVALUE
     * Retourne les elements du tableau.
     *
     * @return Un nouveau tableau Java contenant les elements du
     *         <code>TableauPartiel</code>.
     */
    public E[] elements(){
        return null;
    }

    /**
     * Remplace toute les occurences d'un element par un nouvel
     * element.
     *
     * Cette methode utilise <code>equals</code> pour trouver
     * les occurences de l'element a remplacer.
     *
     * @param ancien L'element a trouver.
     * @param nouveau L'element qui remplace l'element trouve.
     * @throws ElementNonPresentException Lance si l'element n'est pas
     *         dans le tableau.
     */
    public void remplacer( E ancien, E nouveau ) throws ElementNonPresentException {
        int count = 0;
        for ( int i = this.debut; i <=this.fin; i++) {
            if (this.tableau.get(i).equals(ancien)) {
                this.tableau.set(i, nouveau);
                count++;
            }
        }
        if (count == 0) {
            throw new ElementNonPresentException();
        }
    }
}
