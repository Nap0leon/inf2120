
public class ElementNonPresentException extends Exception {
    private static final long serialVersionUID = 1L;

    public ElementNonPresentException(){
        super();
    }

    public ElementNonPresentException( String mssg ) {
        super( mssg );
    }
}
