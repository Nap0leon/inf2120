---
title: "Document de révision examen Intra"
author: [Maxime Masson]
date: "2019-03-27"
keywords: [INF2120, JAVA]
titlepage: true
titlepage-color: "191919"
...

# Révision examen Intra Hiver 2019

## Les fonctions de ArrayList

1. add(index, element)
    - This function `inserts` a specified element at the specified index of the list.
2. remove(index)
    - This function `remove` the element at the specified index of the list.
3. get(index)
    - This function `get` the element at the specified index of the list.
4. set(index, element)
    - This function `replaces` the element at the specified index of the list for the specified element.
5. contains(object)
    - This function return `true` if the specified element (object) is in the list.
6. size()
    - This function return the number of element in the list.
7. isEmpty()
    - This function return `true` if the list contains 0 element.

<br>

## Héritage

`Extends` : On utilise cette annotation pour appelé le lien d'héritage avec une autre classe. Exemple : 

```java
class B extends A {
    // code
}
```

La classe B hérite de A...B est donc une `sous-classe`.

<br>

## Classe **Abstraite**

Concept important dans l'utilisation des classes. 

La classe `abstract` est utilisé comme base pour les `sous-classes`. Les méthodes sont hérité et implémenter.

```java
public abstract class Person {
    private String name;
    private String gender;

    //abstract method
    public abstract void work();
}

public class Employee extends Person {
    private int empId;

    @Override
    public void work() {
        if (empId == 0) {
            System.out.println("Not working");
        } else {
            System.out.println("working as employee");
        }
    }
}
```

**Override** indique au compilateur que nous essayer d'écraser une méthode d'une superclass, dans l'Exemple ci-dessus, la méthode `work()`.

C'est une sécurité, une erreur sera lancé si nous écrasons mal la méthode.

### Méthode Abstract = Classe Abstract
Doit être déclarée `Abstract` une class qui contient une `méthode asbtract`.

Doit être déclarée `Abstract` une **sous-class** qui n'implémente pas une méthode `abstract` de **sa superclass**.

### Pourquoi utiliser une class abstract ?

- Empêcher l'instanciation de cette class
- Regrouper des caractéristiques commune mais dont l'instance de cette classe ne fait pas de sens OU n'est pas concret
- Forcer la définition des méthodes (Pas besoin d'utiliser `instanceOf`).

*exemple*
```java
class abstract IndividuUqam {

}

class EtudiantUqam extends IndividuUqam {

}
class EmployeUqam extends IndividuUqam {

}
```

<br>

## Les Interfaces

Une `Interface` est un type (comme toute class) qu'on dit Abstrait.
Tout comme une classe abstract, on ne peut pas instancier une interface.

Annotation : `implements`

*exemple*
```java
class A implements Interface1 {
    // Code
}
```

### Ce que peut contenir une interface
1. Des méthodes d'instances publiques abstraites
2. Des méthodes publiques de class (static)
3. Des méthodes par défaut (`default`)
4. Des constantes de class publique (Implicitement `public final static`)
5. Des types imbriqués (`Nested types`)

TOUTES LES MÉTHODES SONT IMPLICITEMENT PUBLIQUE.  On peut omettre le modificateur lors de leur déclaration.

### Les différences entre Interface et Class
- Ne peut pas être instancié
- Ne peut pas avoir de constructeur
- Chaque méthode sont abstract
- Ne peut contenir que des variables `static` et `final`
- Est `implémentée` par une class et non `hérité`
- Peut hérite d'autre interface

### Implémentation d'une interface
1. On peut implémenter plusieurs interfaces en les énumérants (virgule)
2. Une classe qui `n'implémente pas` toutes les méthodes des interfaces implémentées doit être déclaré `abstract`.
3. Une classe peut hériter d'une autre classe ET implémenter des interfaces

### Utilisation des variables d'un type interface

Comme une interface est un `type` on peut l'utiliser
1. Pour déclarer une variable
2. Comme type de paramètre d'une méthode
3. Comme type de la valeur de retour d'une méthode

***NOTE*** : On peut affecter a une variable de type interface toute `instance d'une classe`.

### **Contrat vs Implémentation**

***Contrat*** = Le quoi (exemple : les services publics offerts)

***Implémentation*** = Le comment (exemple : Manières dont les services sont implémentés).

<br>

## Type Générique

Si on a une **classe generique** et dont le type generique `extends` une interface (`nomClass<T extends interface>`), du coup on peut assumer que la methode de l'interface va etre implementée dans l'objet generique et on peut utiliser les methodes de l'interface sans savoir ce que l'objet est

Les types génériques ne fonctionne pas avec les types natifs.
- Besoin de faire une méthode unique pour chaque type générique
- On écrit les types générique après le nom de la classe

```java
public class Box<T> {
    // T stands for "type"
    private T t;
}
```

### Convention de nom
- E = Element
- K = Key
- N = Number
- T = Type
- V = Value
- S,U,V, etc. = 2nd, 3rd, 4th types

### Instancié une classe de type générique

Pour instancier un objet de type générique, il faut utiliser la notation en diamant `<>`.
*exemple* : 
```java
// Fait référence à la classe Box<T> si haut.
Box<Integer> integerBox = new Box<>();
```

### Multiple type générique

Une class générique peut avoir plusieurs type. On les énumére en les séparant par des virgules.

```java 
public interface Pair<K,V> {
    public K getKey();
    public V getValue();
}

class OrderedPair<K, V> implements Pair<K,V> {
    private K key;
    private V value;

    public OrderedPair(K key, V Value) {
        this.key = key;
        this.value = value;
    }
}

// Permet ainsi l'instanciation d'un objet de la classe avec plusieurs paramètre
// Fait référence à la classe Box plus haut
OrderedPair<String, Integer> p1 = new OrderedPair<>("Evenb", 8);
OrderedPair<String, Box<Integer>> p = new OrderedPair<>("primes", new Box<Integer>(...));
```

<br>

## ArrayList

Une `ArrayList` est en fait un `tableau dynamique`. C'est un **contenant homogène à taille variable**. Tous les éléments ont le même type.

### Déclarer un ArrayList
```java
ArrayList<Integer> list = new ArrayList<>();
```

## Comparable

Un `comparable` est une interface.

Pour faire une comparaison, nous pouvons utiliser la méthode `.equals`.

Cependant, pour faire une comparaison, par exemple, plus grand ou plus petit, nous devons utiliser l'interface `Comparable`.

### L'unique method : compareTo(T o)

Compare l'objet en question avec l'objet spécifié dans la méthode.
- Si l'objet est `plus grand`, return 1
- Si l'objet est `egal`, return 0
- Si l'objet est `plus petit`, return -1

Cette méthode est utile lorsque nous voulons trié les objets dans un certain ordre.

*Exemple*
```java
class Student implements Comparable {
    public Student(String n, int gr, double g) {
        name = n;
        grade = gr;
        gpa = g;
    }

    public int compareTo(Object temp) {
        Student other = (Student) temp;
        if (getGrade() > other.getGrade()) {
            return 1;
        } else if (getGrade() < other.getGrade()) {
            return -1;
        } else {
            return 0;
        }
    }
    
}

public class Interface {
    public static void main(String[] args) {
        Student s1 = new Student("Joe",12,3.5);
        Student s2 = new Student("Bob",12,3.7);

        int result = s1.compareTo(s2);
        if (result < 0) {
            return "s1 comes before s2";
        } else if (result > 0) {
            return "s2 comes before s1";
        } else {
            return "s1 is equal to s2";
        }
    }
}
```

***En bref*** : l'interface Comparable est très utile pour comparer des `Objets`.

<br>

## Class Objet

La classe objet est la `superclass` de toutes les classes.

### getClass()

Cette méthode retourne le `type dynamique de l'objet` sur lequel elle est appelée.

```java
public final class<?> getClass()

// Exemple
Number n = 0;
Class<? extends Number> c = n.getClass();
```

Le type de retour est un type **Class**.  Pour obtenir le `nom de la classe` sous forme de chaine, on doit appeler `getName()` ou `getSimpleName`.

```java
// Dans le package test

AnimalDomestique a = new Oiseau();
a.getClass() // retourne la classe du type dynamique de a (Oiseau);
a.getClass().getName(); // retourne la chaine représentant le type dynamique de a

//affiche : class test.Oiseau
System.out.println(a.getClass().toString());
//affiche: test.Oiseau
System.out.println(a.getClass().getName());
//affiche: Oiseau
System.out.println(a.getClass().getSimpleName());
```

### toString()

Cette méthode retourne une représentation sous forme de chaine de charactère de cet objet.
De base, la `chaine retournée est constituée de : ` 
1. le nom de la classe de l'objet
2. Une représensation hexa du hash code de l'objet

```java
getClass().getName() + '@' + Integer.toHexString(hashCode());
```

***RECOMMENDATION*** : Il est recommandé de redéfinir la méthode `toString()` de dans chaque class afin de retourner une chaine personnalité représentant textuellement une instance de cette class.

### equals()

Cette méthode indique si un objet est égal à l'objet sur lequel on applique la méthode.

1. Compare les `pointeurs` (référence)
2. Return `true` si x et y pointent sur le `même espace mémoire`.

***RECOMMENDATION*** : Il est recommandé de redéfinir la méthode `equals()` dans les classes dont on veut comparer les instances sur leur attributs (et non leur référence).
    À NOTER
        Cette méthode implémente une `relation d'équivalence` sur des objets non null. La redéfinissions doit donc respecter : 
        1. Réflexive
        2. Symétrique
        3. Transitive
        4. Consistante

*exemple*
```java
public boolean equals (Object autreAnimal) {
    return
    //vérifie que autreAnimal n'est pas null sinon return false
    autreAnimal != null &&

    //verifie que les deux objets comparés sont de meme classe sinon return false
    this.getClass().equals(autreAnimal.getClass()) &&

    //verifie l'egalite des attributs sinon return false
    this.nom.equals(((AnimalDomestique).autreAnimal).nom) &&
    this.age == ((AnimalDomestique)autreAnimal).age;
}
```

<br>

## Iterable && Iterator

Iterator est une interface de type E (type of elements). Cette interface prend la place de `Enumeration` du framework Java et itère sur une `Collection`. Cependant, elle se distingue par : 

1. L'itérateur permet d'enlever des éléments lors de l'itération.

### hasNext()

Cette méthode retourne vrai s'il y a un élément suivant. Utilse pour savoir s'il faut continuer à itérer.

*exemple*
```java
while(hasNext()) {
    // Code
}
```

### next()

Cette méthode retourne l'élément suivant dans l'itération.  S'enchaîne bien avec la méthode `hasNext()`.

*exemple*
```java
while (hasNext()) {
    action.accept(next());
}
```

### remove()

Cette méthode permet de retirer le dernier élément itérés.

### forEachRemaining(Consumer<? super E> action)

Cette méthode ne retourne rien. Elle performe une série d'action spécifié sur `TOUS` les éléments restant.


### Utilisation

```java
Iterable <E> c;
    ...
    Iterator<E> it = c.iterator();
    while (it.hasNext()) {
        E o = it.next(); // o a pour valeur un objet de la collection
        ...
    }
```

### Notation avancé de ForLoop

Pour simplifier le code, il est possible d'écrire la syntaxe `ForLoop` de façon simplifié, aussi très utile lors d'itération. (Iterator/Iterable).

```java
for (String y : morethings)
    list2.add(y);

```
- Boucle sur `moreThings` qui est la `list1`.
- Les items de moreThings sont nommé `y`.
- On ajoute `y` à la `list2`.

<br>

## Les class de Fonction

Il existe 4 classes de fonction importante (type d'interface)

1. Function<T,R> (T: type argument, R: type retour)
2. Predicate<T> (return boolean, fonction qui sert à tester)
3. Supplier<T> (Contient une méthode get, return une valeur mais reçoint aucun argument)
4. Consumer<T> (Fonction qui accepte des arguments et return rien; println est un consumer)

<br>

