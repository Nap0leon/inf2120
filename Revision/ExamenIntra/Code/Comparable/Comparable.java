public interface Comparable<T> {
    public enum Ordre {
        PLUS_PETIT, EGAL, PLUS_GRAND
    }

    Ordre comparer(T v2);
}