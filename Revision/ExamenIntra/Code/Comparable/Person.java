public class Person implements Comparable<Person> {
    private int age;
    
    public Person(int age) {
        this.age = age;
    }

    public Ordre comparer(Person temp) {
        if (this.age > temp.age) {
            return Ordre.PLUS_GRAND;
        } else if (this.age < temp.age) {
            return Ordre.PLUS_PETIT;
        } else {
            return Ordre.EGAL;
        }
    }
}