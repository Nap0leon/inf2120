import java.util.Scanner;

public abstract class Animaux {
	private boolean aggressif ;
	private boolean vivant ;
	private boolean mort ;
	private int nbAttaque ; 
	private int chiffrePourEnum ;
	protected Enum monEnum;
	
	
	protected void setmonEnum(Enum monEnum) {
	 this.monEnum = monEnum ;	
	}
	
	public Animaux() {}
	
	public Animaux(boolean etat,boolean agressif,int Cenum) {
		if(etat == true )
			this.vivant = etat ;
		else
			this.mort = etat ;
		this.aggressif = agressif ;
		this.setChiffrePourEnum(Cenum);
			
	}
	public String attaquer () {
		String result ;
		if( aggressif == true) {
		++nbAttaque ;
		result = " Votre animal vient de faire une attaque voici son nombre d'attaque " + nbAttaque ;
	 }else
		 result = "Votre animal est incapable d'attaquer" ;
		
		return result ;
	}
	public void seFaireDomestiquer() {
		aggressif = aggressif == true ? false : false ;
	}
	@Override
	public boolean equals(Object o) {
		boolean result = false ;
		
		if(null != o && o instanceof Animaux) {
			 Animaux temp = (Animaux)o ;
			 result = temp.aggressif == this.aggressif && (temp.mort == this.mort || temp.vivant== this.vivant ) && temp.nbAttaque == this.nbAttaque;
		}
		
		return result ;
		

	}

 	public String environnement() {
		return  " 	Due to thanos and his guantlet		\n" + 
				" .              +   .                .   . .     .  .\n" + 
				"                   .                    .       .     *\n" + 
				"  .       *                        . . . .  .   .  + .\n" + 
				"            \"You Are Here\"            .   .  +  . . .\n" + 
				".                 |             .  .   .    .    . .\n" + 
				"                  |           .     .     . +.    +  .\n" + 
				"                 \\|/            .       .   . .\n" + 
				"        . .       V          .    * . . .  .  +   .\n" + 
				"           +      .           .   .      +\n" + 
				"                            .       . +  .+. .\n" + 
				"  .                      .     . + .  . .     .      .\n" + 
				"           .      .    .     . .   . . .        ! /\n" + 
				"      *             .    . .  +    .  .       - O -\n" + 
				"          .     .    .  +   . .  *  .       . / |\n" + 
				"               . + .  .  .  .. +  .\n" + 
				".      .  .  .  *   .  *  . +..  .            *\n" + 
				" .      .   . .   .   .   . .  +   .    .            +\n" ;
	}
	
 	
 	public String toString() {
 		String etat = this.vivant == true ? "vivant" : "mort" ;
 		String comportement = this.aggressif == true ? "une bête sauvage." : " un animal de compagnie." ;
 		return "Je suis un "+ this.getClass().getSimpleName() + ",voila mon etat :" + etat + " et je suis "+ comportement 
 				+ " Aujourd'hui il fait un temps :"+ monEnum+ ".";
 	}
 	
 	
 	
 	



	protected boolean isAggressif() {
		return aggressif;
	}

	protected void setAggressif(boolean aggressif) {
		this.aggressif = aggressif;
	}

	protected boolean isVivant() {
		return vivant;
	}

	protected void setVivant(boolean vivant) {
		this.vivant = vivant;
	}

	protected boolean isMort() {
		return mort;
	}

	protected void setMort(boolean mort) {
		this.mort = mort;
	}

	protected int getNbAttaque() {
		return nbAttaque;
	}

	protected void setNbAttaque(int nbAttaque) {
		this.nbAttaque = nbAttaque;
	}

	protected int getChiffrePourEnum() {
		return chiffrePourEnum;
	}

	protected void setChiffrePourEnum(int chiffrePourEnum) {
		Scanner sc = new Scanner(System.in) ; 
		while(chiffrePourEnum > 2 || chiffrePourEnum< 0){
		System.out.println("Veuillez faire un Set pour avoir votre météo, entrer un chiffre compris entre 0 et 2 s'il vous plait. ");
		chiffrePourEnum = sc.nextInt();
		}
		sc.close();
		;
	}


}