interface Vehicle {
    void changeGear(int a);
    void speedUp (int a);
    void applyBrakes (int a);
}

// L'interface regroupe ce que toutes les classes qui l'implement ont en commun