import java.util.*;

public class Main {
    public static void main(String[] params) {
        String[] things = {"Eggs", "lasers", "hats", "pie"};
        List<String> list1 = new ArrayList<String>();

        //add array items to list -- Loop through entire array and for each arrayItem add x
        for (String x : things)
            list1.add(x);

        String[] morethings = {"lasers", "hats"};
        List<String> list2 = new ArrayList<String>();

        //Loop through morethings and for every item in list1, add them in list2
        for (String y : morethings)
            list2.add(y);

        // Function to print the item from list(x)

        for (int i = 0; i < list1.size(); i++) {
            System.out.printf("%s ", list1.get(i));
        }

        editlist(list1, list2);
        System.out.println();

        for (int i = 0; i < list1.size(); i++) {
            System.out.printf("%s ", list1.get(i));
        }

    }

    public static void editlist(Collection<String> l1, Collection<String> l2) {
        Iterator<String> it = l1.iterator();
        while(it.hasNext()) {
            if(l2.contains(it.next())) {
                it.remove();
            }
        }
    }
}

