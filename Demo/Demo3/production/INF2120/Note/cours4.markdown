# Cours 4 - JAVA II

### Résumé du cours précédent
Nous avons vue les classes `Abstract` ainsi que les méthodes abstract et comment les hériter.

Nous avons vue les type `génériques`.


## Opérateur ternaire

Un opérateur ternaire prend 3 éléments. S'écrit de la façon suivante : 
```java
int x = 10;
int y = 12;
int z = x < y ? : x : y;


if boolean ? return x : else return y
```


## Type Générique / Paramétrique

Dans une classe, on écrit les type paramétrique après le nom de la classe
```java
public class Pair<T1, T2> {
    T1 premier;
    T2 deuxieme;
}
```

Avec cette classe, nous pouvons ensuite en faire appel de la façon suivante dans le `main`.
```java
public static void main( String[] params) {
    Pair< String, Integer > p = new Pair<>("allo", 4);
}
```

Les méthodes à l'intérieur de `classe avec type générique` peuvent elles-aussi avoir des types génériques qui leur sont propre
```java
public <E> void m(E e) {
        
}
```

## ArrayList / Collection

Le arraylist ressemble au tableau. Cependant, le ArrayList est un `tableau à taille variable`, soit un Tableau Dynamique.

**Contenant homogène à taille variable**

Il est possible de modifier la taille du tableau n'importe quand.

Le `ArrayList` doit être homogène, tous les éléments **ont le même type**.

Le arraylist possède plusieurs opérations.
```java 
boolean remove(Object o)
boolean add(Object o)
boolean isEmpty();
boolean contains(Object o)
boolean removeAll(Collection<?> c); // Tout supprimer
boolean addAll(Collection<? extends E> c) //Ajoute une collection a une autre
```

Un des premiers rôle des liste est **d'indexer** les éléments
```java
void add(int index, E element);
```
Qui nous permet d'ajouter un élément a un index précis.  En ce sens, nous pouvons aussi aller chercher des éléments avec certaines opérations tel que
```java
E get(int index) // Return l'élément à l'index spécifier
int indexOf(Object o) // Return l'index de la première occurance dans la liste OU -1 si l'élément n'existe pas.
```

### How to declare an ArrayList
```java
 public static void main(String[] params) {
        ArrayList<Integer> a1 = new ArrayList<>();
        a1.add(12);
        a1.get(0);
        if ( a1.isEmpty() ) {

        }
}
```

### Function to know for exams
```java
add()
remove()
get()
set()
size()
isEmpty()
contains()
```

## Interface

Is a collection of abstract methods. **Similar to a class***.

It may contain constants, default methods, static methods, and nested types.

To use an interface, we change the syntaxe into the principal class.
```java 
public interface class Forme2D {
    public Forme2D() {
    }
    public abstract double calculerAire();
}
```
```java
public class Circle extends Object {
    implements forme2D {
        protected double rayon;

        @Override
        public double aire() {
            return Math.PI * rayon * rayon;
        }
    }
}
```

The word `implements` here is for the implementation of the interface. 


## Comparable

`function : ` compareTo();

Exemple
```java
public int compareto(Circle arg0) {
    return new Double(rayon).compareTo(arg0.rayon);
}
```
The function return a int.