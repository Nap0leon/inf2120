import java.lang.reflect.Array;
import java.util.ArrayList;

public class Principale {


//    public static <T> PeutEtre<Integer> trouverElement(T[] a_tableau, T a_element) {
//        PeutEtre<Integer> resultat = null; //Declare object PeutEtre a null
//
//        int i = 0; // Déclare le compteur pour itéré dans le tableau
//
//        /**
//         * Incrémente le compteur jusqu'a ce qu'il soit plus grand que la compteur du tableau
//         * OU
//         * qu'un élément soit égal un élément du tableau à l'index i
//         */
//        while( i < a_tableau.length && (! a_element.equals(a_tableau[i]))) {
//            i++;
//        }
//
//        /** Avec la boucle while, permet de créer QQchose si élément est inclus dans tableau
//         * car
//         * Sinon i > tableau
//         */
//        if (i < a_tableau.length) {
//            resultat = new QQChose<Integer>(new Integer(i));
//        } else {
//            resultat = new Rien<Integer>();
//        }
//        return  resultat;
//
//    }

    /**
     *
     * @param depart Première valeur de la liste
     * @param fin Dernière valeur de la liste
     * @param nbrInterval Faire +1 pour avoir le nombre d'élément de la liste. Aide aussi à déterminer l'intervalle entre 2 élément en faisant (fin - départ / nbrInterval)
     * @return
     */
    public static ArrayList<Double> tweens(double depart, double fin, int nbrInterval) {
        ArrayList<Double> a = new ArrayList<>();
        int longueur = nbrInterval + 1;
        double interval = fin - depart / nbrInterval;
        a.add(depart);
        a.add(fin);
        for (int i = 1; i < longueur; i++) {
            double nombre = depart + interval;
            a.add(i, nombre);
        }
        return a;
    }

    public static void main(String[] param) {
        ArrayList<Double> test = tweens(1.0, 3.0, 4);
        System.out.println(test);
    }
}
