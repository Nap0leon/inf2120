/** Classe avec type générique qui hérite de la classe PeutEtre avec type générique.*/

public class Rien <T> extends PeutEtre<T> {
    public Rien() {}

    @Override
    public boolean estQQChose() {
        return false;
    }
    @Override
    public boolean estRien() {
        return true;
    }
    @Override
    public T qQChose() throws ARien {
        throw new ARien();
    }
}
