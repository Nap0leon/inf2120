public class Fraction implements Nombre<Fraction> {
    private int num;
    private int denum;

    public Fraction(int num, int denum) {
        this.num = num;
        this.denum = denum;

        reduit();
    }

    public int getNum() {
        return num;
    }
    public int getDenum() {
        return denum;
    }

    private void reduit() {
        int pg = pgcd(num,denum);
        num /= pg;
        denum /= pg;
    }

    private static int pgcd(int a, int b) {
        while(b != 0) {
            int temp = a % b;
            a = b;
            b = temp;
        }
        return a;
    }

    @Override
    public Fraction add(Fraction x) {
        int n = num * x.denum + denum * x.num;
        return new Fraction(n, denum * x.denum);
    }
    @Override
    public Fraction sub(Fraction x) {
        int n = num * x.denum - denum * x.num;
        return new Fraction(n, denum * x.denum);
    }
    @Override
    public Fraction mul(Fraction x) {
        int n = num * x.num;
        return new Fraction(n, denum * x.denum);
    }
    @Override
    public Fraction div(Fraction x) {
        int n = num * x.denum;
        return new Fraction(n, denum * x.num);
    }

    public String toString() {
        return num + "/" + denum;
    }
}