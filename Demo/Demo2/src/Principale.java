public class Principale {

    public static void main(String[] params) {

        Cercle c = new Cercle( 3);
        Rectangle r = new Rectangle(5,6);
        Sphere s = new Sphere(5);
        CylindreDroit cd = new CylindreDroit(c, 5);

        Forme3D Tableau [] = new Forme3D[2];
        Tableau[0] = s;
        Tableau[1] = cd;

        for (Forme3D forme : Tableau) {
            System.out.println(forme + " et donc, un volume de " + forme.calculerVolume());
        }
    }
}
