public class CylindreDroit extends Forme3D {

    private Forme2D base;
    private double hauteur;

    public CylindreDroit(Forme2D base, double hauteur) {
        this.base = base;
        this.hauteur = hauteur;
    }

    @Override
    public double calculerVolume() {
        return base.calculerAire() * hauteur;
    }

    public String toString() {
        return "Ce cylindre a une aire de base de " + base.calculerAire() + " et une hauteur de " + hauteur;
    }
}
