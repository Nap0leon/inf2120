public class Sphere extends Forme3D {
    private double rayon;

    public Sphere(double r) {
        this.rayon = r;
    }

    @Override
    public double calculerVolume() {
        return (4.0/3) * Math.PI * Math.pow(rayon,3);
    }

    public String toString() {
        return "Cette sphere a un rayon de " + rayon;
    }
}
