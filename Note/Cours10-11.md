---
title : Cours 10 & 11 - JAVA II
author: [Maxime Masson]
titlepage : true
---

# Resume

- PILE
- ALGORITHM
    
    *Polynomial*
    - Constant
    - Log
    - Linear
    - n log n
    - Quad

    *Non polynomial*
    - Exponential

# Introduction

- Recherche
- Tri

## Algorithm de recherche

    recherche( fouille, find )
Collection de valeur (homogene).
- Est-ce qu'une valeur est dans la collection ?
- Quel est la position de l'element ?
    - position ou une reference.
    = pas present : exception, position impossible.
- modifier une case. (set, replace).

```java
public class Recherche {
    /*
     * Recherche lineaire
     * soit un tableau, trouver la position d'un element.
     * ex : 
     * Integer [] tab = {4, 2, 6, 43, 7, 45};
     * int p = rechercheLineaire( tab, 2 );
     * -- Devrait retourner 1.
     */

    Rechercher première occurence
    /*
     * rechercheLineaire( tableau, element ) : int
     *  i <- 0
     *  tantque ( i < tab.taille et tab[i] != element )
     *      i++
     *  
     *  retourner i;
     *
     */

    Rechercher dernière occurence
    /*
     * rechercheLineaire( tableau, element) : int
     * i <- tab.taille -1
     *  tantque ( i >= 0 et tab[i] != element )
     *      i--;
     * 
     *  retourner i;
}
```

Pour compter la complexité, il suffit de compter le nombre d'opération

-> Pour la recherche #1
|<-| + | < = | != |
|---|---|---| --- |
|1| n | n + 1 | n |
|1||||

Dans le pire cas, nous avons 4 + 3n opérations totales.

    Complexité (meilleur) : O(1)
    Complexité (cas moyen) : O(n)
    Complexité (pire cas) : O(n) 


## Algorithme de recherche binaire

L'idée de mettre en ordre une collection accélère énormément le complexité des algorithmes.

Pour ce faire, on divise à chaque tour de boucle la collection en 2. On commence avec
- debut = 0
- fin = taille - 1

On rapproche les index à chaque tour de boucle pour trouver l'element

***IMPORTANT*** : La collection doit déjà être trié pour que la recherche binaire fonctionne.

```java
/**
 * rechercheBinaire( tab, element ) : int
 * debut <- 0
 * fin <- taille - 1
 * 
 * tantque( debut < fin )
 *      invariant : 
 *      si l'element est dans le tableau, alors sa position est entre debut et fin.
 * 
 *      milieu <- ( debut + fin ) / 2
 *      si (element <= tab[milieu] )
 *      alors
 *          fin <- milieu
 *      sinon
 *          debut <- milieu + 1
 * si tab[debut] != element
 * alors debut <- -1
 * 
 * retourner debut
 */
``` 

Complexité de l'algorithme

    1 = 1/2^k N
    2^k = N
    log 2^k = log N
Dans tous les cas : O(log n)


## Algorithme en Java
```java
public static <E> int rechercheLineaire( E [] tab, element ) {
    // Si type generique, ne pas oublier de les déclarer dans la méthode
    // Aussi utiliser des getter et equals pour ne pas comparer les pointeurs
    int i = 0;
    while ( i < tab.length && tab[i].equals( element ) ) {
        i++;
    }
    if ( tab.length == i ) {
        i = -1;
    }
    return i;
}
```

```java
public static <E extends Comparable<E> > int rechercheBinaire( E [] tab, element ) {
    int debut = 0;
    int fin = tab.length - 1;
    while ( debut < fin ) {
        int milieu = (debut + fin) / 2;
        // compareTo retourne 0 si egal, -1 si plus petit, 1 si plus grand
        if ( element.compareTo(tab[milieu]) <= 0 ) {
            fin = milieu;
        } else {
            debut = milieu - 1;
        }
    }
    if ( element.compareTo( tab[debut] ) != 0 ) {
        debut = -1;
    }

    return debut;
}
```

## Algorithme de tri

Tri 
- Collection, ordonnée (séquentielle)
- Modifier l'ordre des elements de la collection
    - Echange.
- Pour tout i, j : i < j -> tab[i] < tab[j]
- Performance
    1. Nombre d'echange (deplacement)
    2. Nombre de comparaison : O(n log n)  -> Ne sera jamais dépassé

```java

/**
 * Tri Sélection
 *     Toujours 2 boucles imbriquées (Boucle externe, boucle interne)
 *     Externe : Où rendu pour trouver le minimum ?
 *     Interne : Trouve le minimum
 * Tri Insertion
 * Tri Bulle
 * Tri Rapide (Quicksort)
 **/

```

## Algorithme de tri selection

```java

public static < E extends Comparable<E> > void triSelection(E [] tab) {
    for (int i = 0; i < tab.length -1 ; i++) {
        E minima = tab[i];
        int position = i;
        for (int j = i +1; j < tab.length; i++){
            if (tab[j].compareTo( minima ) < 0 ) {
                minima = tab[j];
                position = j;
            }
            tab[position] = tab[i];
            tab[i] = minima;
        }
    }
}

public static void main (String[] args) {
    Integer[] t = {6, 3, 1, 9, 2, 4, 5, 7, 8, 0};

    triSelection(t);

    for (int i = 0; i < t.length ; i++) {
        System.out.print(t[i] + " ");
    }
}

```

## Algorithme de Tri avec Insertion

Le principe est de vérifier si l'élément suivant est plus grand que l'élément déclaré en tant que "temp".
Si non, déplacer cet élément au début et le déclarer comme "temp".

```java
TriInsertion( tab ) 
    pour i de 1 a tab.taille - 1
        temps <- tab[i]
        j < i - 1
        tantque 0 <= j et temp < tab[j]
            tab[j+1] <- tab[j]
            j--
        tab[j+1] <- temp
        
public static < E extends Comparable<E> > void triInsertion(E [] tab) {
        for (int i = 1; i < tab.length; i++) {
            E temp = tab[i];
            int j = i -1;
            while ( 0 <= j && temp.compareTo(tab[j]) < 0 ) {
                tab[j+1] = tab[j];
                j--;
            }
            tab[j+1] = temps;
        }
    }
```

## Algorithme de Tri avec Bulle

Complexité de tri bulle : O(n^2)

```java
public static <E extends Comparable<E> > void triBulle(E[] tab) {
    for (int i = 0; i < tab.length; i++) {
        for (int j = 0; j <tab.length -1; j++) {
            if (tab[j].compareTo(tab[j+1]) > 0 ) {
                E temp = tab[j];
                tab[j] = tab[j+1];
                tab[j+1] = temp;
            }
        }
    }
}

```

## Algorithme de tri rapide (Quicksort)

Prendre un tableau et le diviser en deux. Toutes les valeurs de gauche doivent être plus petites que les valeurs à droite.

Utilise le principe de partition. On choisit la valeur à l'indice milieu qu'on nomme `valeur pivot`.

### Complexité

- ***Meilleur cas*** : I(n log n)
- ***cas moyen*** : O(n log n)
- ***pire cas*** : O( n^2 )

**Invariant** : Déterminer des bornes <> que le pivot

```java
triRapide(tab, debut, fin) // Quicksort
    inf <- debut - 1;
    sup <- fin + 1;
    tantque inf < sup
        invariant : pour tout i <= inf, tab[i] < pivot
                    pour tout sup <= i, pivot < tab[i]
        tantque inf < sup et tab[inf] <= pivot
            inf++
        tantque inf < sup et pivot < tab[sup]
            sup--
        si inf < sup alors 
            tab[inf] <-> tab[sup]
    triRapide(tab, debut, inf - 1)
    triRapide(tab, inf, fin)


// MATIERE A EXAMEN
public static <E extends Comparable<E>> void triRapide(E[] tab, int debut, int fin) {
    if (debut<fin) {
        E pivot = tab[(debut + fin) / 2];
        int i = debut - 1;
        int j = fin + 1;

        while ( i < j) {
            do j--; while(tab[j].compareTo(pivot) > 0);
            do i++; while(tab[i].compareTo(pivot) < 0);

            if (i<j) {
                E temp = tab[i];
                tab[i] = tab[j];
                tab[j] = temp;
            }
        }
        triRapide(tab, debut, j);
        triRapide(tab,j + 1; fin);
    }
}    
```

### Algorithme de partition

Définit 2 bornes (Soit minimum et maximum, debut/fin) et rapproche les bornes. Lorsque les bornes se touchent
le tableau est trié.

### Alternative

```java
public static < E extends Comparable< E > > void triRapide(E [] tab) {
    triRapide(tab, 0, tab.length - 1);
}
```
