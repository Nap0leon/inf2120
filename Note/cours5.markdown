# Advanced ForLoop notation

```java
for (String y : morethings)
    list2.add(y);
```
Loop through morethings and for every item (y) in list1, add them in list2


# Classe de fonction importante

Il y a 4 classes de fonction importante (type d'interface)

1. **Function<T, R>** (T: type argument, R: type retour)
2. **Predicate< T >** (return boolean, fonction qui sert à tester)
3. **Supplier< T >**  (Contient une méthode get, retourne une valeur mais reçoit aucun argument)
4. **Consumer< T >** (Fonction qui accepte des arguments et retourne rien ; println est un consumer et renvoie quelque chose à l'écran)


Dans une interface, possible de faire des classes pour implementer toutes les méthodes à l'intérieur

OU

Faire une classe instantanné à même le main (Code/Fonction/Principal.java)

```java
Function<Integer, Integer> f1 = new class F1 implements Function<>() {};

Function<Integer, Integer> f1 = new Function<Integer, Integer>() {
		@Override
		public Integer apply(Integer x) {
			return 4 * x * x - 7;
		}
    };
```

# Notation Lambda (Réécrire une fonction directement dans le langage)

```java
//Exemple
Function<Integer, Integer> f3 = x -> 3 * x - 1;
Function<Integer, Integer> f4 = (x) -> 3 * x - 1;

Function<Integer, Integer> f5 = x -> {
    int i = 0;
    return i;
};

Consumer<Integer> c = (x) -> {
	System.out.println("a : " + x);
};
```

- Avec 1 argument, pas obligé de mettre les parenthèses
- S'il y a plusieurs arguments, mettre des parenthèse et séparés par une virgule
- Pas obligé de mettre le **type** du paramètre


### Signature lambda

```java
Consumer<Integer> c = System.out::println;
c.accept(4);
```
Sert à faire référence à la méthode `println` dans la classe `System` et de **l'utiliser** comme `Consumer`.
- Empêche de faire APPEL à la méthode dans une classe qui utilise déjà.
- `c` est une instance de classe


# FunctionalInterface

Garantit qu'il n'y a **seulement que 1 méthode non définit** (Donc non default, non static).

Seulement 1 méthode *abstract*.

Comme ça que le compilateur reconnaît les Lambda notations.

- Possible de créer nous même une FunctionalInterface

