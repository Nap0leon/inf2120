---
title : Cours 8 - JAVA II
author: [Maxime Masson]
titlepage : true
---

# Resume

## RECURSION
    - FACT
    - FIBO
    - HANOI

## TDA
    - DEFINITION

## LIST
    - Component
    - Sequence
    - Collection
    - Ordered random access
    - Insert/Remove
    - isEmpty
    - size
    - get/set

# Introduction

## File & Pile 

`File` : Sequential, homogeneous collection with a dynamic size.

`FIFO` : First in, first out.

        Head : First element
        Tail : All of the other element
        
        Only the head can be poped

| Type | Methods | Definition |
| --- | --- | --- |
| File< E > | Constructor | Create a new empty File |
| Boolean | isEmpty() | Return true if list is empty |
| E | head() OR Tete() | Return the element at the HEAD index |
| E | push(E) OR Enfiler(E) | Add an element at the end of the File |
| Void | pop() OR Defiler() | Remove the HEAD element |

<br>

`Pile` : Homogeneous. sequential collection of a dynamic size.

`LIFO` : Last in, first out

    Peek : The object at the top of the pile

| Type | Methods | Definition |
| --- | --- | --- |
| Pile< E > | Constructor | Create a new pile (`new Pile()`) |
| Boolean | isEmpty() | Return true if the stack is empty |
| E | peek() OR sommet() | Return the object at the PEEK |
| E | push(E) OR empiler(E) | Add an object on the PEEK |
| Void | pop() OR depiler() | Remove the object at the PEEK |

```java
P = new Pile() -> P.isEmpty()

P.push(e)
P.peak() == e

// If push then pop, stack remains the same
P == R
P.push(e);
P.pop(e);
P == R
```

<br>

### Pile Example
```java
class Pile<E> extends ArrayList<E> {
    public Pile() {
        super();
    }
    // Useless method
    // public boolean estVide() {
    //     return isEmpty();
    // }
    public E peek() throws ExceptionPileVide {
        if (isEmpty()) {
            throw new ExceptionPileVide(msg);
        }
        return get(size() - 1);
    }
    public void push(E element) {
        add(element); // Ajoute automatiquement a la fin
    }
    public E pop() throws ExceptionPileVide {
        if (isEmpty()) {
            throw new ExceptionPileVide(msg);
        }
        return remove(size() - 1);
    }
}
//When inherit from a class, we can use `This` to use the class methods
```

## Implementation

### Library

### Heritage

### Linked list

An array is static.  The Idea of a `LinkedList` is to create a Table with each element of the Table. 

That way, we reference each part to the next one, creating what we call the `LinkedList` 

    [1] -> [2] -> [3] -> [4] ...

If we work with a Stack, the pointer will be called `previous` as it's pointing the precedent element. (Since we stack objects). 
- The first object on the stack is pointing on `Null`.
- A link contains a reference over another link ***(pointer)***

***Example***
```java
class Link<E> { // Maillon
    protected E element;
    protected Link<E> previous;
    //For nth Link
    public Link(E element, Link<E> previous) {
        this.element = element;
        this.previous = previous;
    }
    //If first Link
    public Link(E element) {
        this.element = element;
        this.previous = null;
    }
}

class Pile<E> {
    protected Link<E> peek;

    public Pile() {
        peek = null;
    }
    public boolean estVide() {
        return null == peek;
    }
}
```
