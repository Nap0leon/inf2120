/**
 * Code définissant un Arbre Binaire
 * Nécessite trois méthodes spécifiques - Chercher, Insérer, Supprimer.
 * Voir TreeMap<K,V> dans la Doc
 * @param <K>
 */
public class ABR< K extends Comparable<K> > {
    private class Noeud< K  extends Comparable<K2>> {
        public K2 clef;
        public Noeud< K2 > gauche;
        public Noeud< K2 > droite;

        public Noeud( K2 clef ) {
            this.clef = clef;
        }
    }

    protected Noeud<K> racine;

    // Si arbre vide, null = contenue du constructeur.
    public ABR() {}

    // Méthode 1 - Insérer une clef dans l'arbre
    public void inserer(K clef) {

    }

    // Méthode 2 - Supprimer une clef de l'arbre
    public void supprimer(K clef) {

    }

    // Méthode 3 - Chercher une clef et retourner True/False si elle est ou non présente
    public boolean chercher(K clef) {
        Noeud<K> resultat = null == racine ? null : racine.chercher(clef);
        return null != resultat;
    }
    
    public Noeud<K2> chercher(K2 clef) {
        Noeud<K2> resultat = this;
        int direction = clef.compareTo(this.clef);

        if (direction != 0) {
            if (direction < 0) {
                if (null != gauche) {
                    resultat = gauche.chercher(clef);
                } else {
                    resultat = null;
                }
            } else {
                if (null != droite) {
                    resultat = droite.chercher(clef);
                } else {
                    resultat = null;
                }
            }
        } 
        return resultat;
    }

    /**
     * Parent | courant | resultat
     * -------+---------+---------
     * null   | null    | arbre est vide
     * null   | ! null  | racine contient la clef
     * ! null | null    | clef pas dans l'arbre
     * ! null | ! null  | clef est dans l'arbre
     */
    private Pair<Noeud<K>, Noeud<K>> chercherParentCourant (K clef) {
        Noeud<K> courant = racine;
        Noeud<K> parent = null;
 
        int direction = (null == courant) ? 0 : clef.compareTo(courant.clef) ;
 
        while (null != courant && direction != 0)  {
            if (direction < 0) {
                parent = courant;
                courant = courant.gauche;
            } else {
                parent = courant;
                courant = courant.droite;
            }
            direction = (null == courant) ? 0 : clef.compareTo(courant.clef);
 
        }
 
        return new Pair<> (parent,courant);
    }   
    

    public boolean chercher(K clef) {
        Pair<Noeud<K>, Noeud<K>> r = chercherParentCourant(clef);

        if (null == resultat.deuxieme) {
            Noeud<K> nouveau = new Noeud<>(clef);
            if (null == resultat.premier) {
                racine = new Noeud(clef);
            } else {
                int direction = clef.compareTo( resultat.premier.clef );
                if (direction < 0 ) {
                    resultat.premier.gauche = nouveau;
                } else {
                    resultat.premier.droite = nouveau;
                }
            }
        }
        return null != r.deuxieme;
    }

    public boolean estVide() {
        return null == racine;
    }

}