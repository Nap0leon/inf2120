class Link<E> { // Maillon
    protected E element;
    protected Link<E> previous;
    //For nth Link
    public Link(E element, Link<E> previous) {
        this.element = element;
        this.previous = previous;
    }
    //If first Link
    public Link(E element) {
        this.element = element;
        this.previous = null;
    }
}

class Pile<E> {
    protected Link<E> peek;

    public Pile() {
        peek = null;
    }
    public boolean estVide() {
        return null == peek;
    }
}