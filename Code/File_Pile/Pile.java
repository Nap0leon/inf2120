class Pile<E> extends ArrayList<E> {
    public Pile() {
        super();
    }
    // Methode inutile dans ce cas
    // public boolean estVide() {
    //     return isEmpty();
    // }
    public E peek() throws ExceptionPileVide {
        if (isEmpty()) {
            throw new ExceptionPileVide(msg);
        }
        return get(size() - 1);
    }
    public void push(E element) {
        add(element); // Ajoute automatiquement a la fin
    }
    public E pop() throws ExceptionPileVide {
        if (isEmpty()) {
            throw new ExceptionPileVide(msg);
        }
        return remove(size() - 1);
    }
}
//We we inherit from a class, we can use `This` to call the class method