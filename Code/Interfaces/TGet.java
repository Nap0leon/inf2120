import java.util.function.Supplier;

public class TGet implements Supplier< Integer > {
    protected int i = 0;
    @Override
    public Integer get() {
        i++;
        return i;
    }
}
