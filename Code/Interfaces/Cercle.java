public class Cercle implements Forme2D, Eq<Cercle>, Ord<Cercle> {
	protected double rayon;
	
	public Cercle( double r ) {
		rayon = r;
	}
	
	@Override
	public double aire() {
		return Math.PI * this.rayon * this.rayon;
	}

	@Override
	public boolean estEgal( Cercle a2 ) {
		return rayon == a2.rayon;
	}

	@Override
	public boolean estPlusPetit(Cercle a2) {
		return rayon < a2.rayon;
	}
}