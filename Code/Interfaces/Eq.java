public interface Eq< T > {
	boolean estEgal( T a2 );
	default boolean NEstPasEgal( T a2 ) {
		return ! estEgal( a2 );
	}
}