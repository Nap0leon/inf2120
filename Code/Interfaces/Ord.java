public interface Ord< T > extends Eq< T >{
    boolean estPlusPetit( T a2 );
    default boolean estPlusGrand( T a2 ) {
    	return ! estPlusPetitEgal( a2 );
    }
    default boolean estPlusPetitEgal( T a2 ) {
    	return estPlusPetit( a2 )
    			||
    		   estEgal( a2 );
    }
    default boolean estPlusGrandEgal( T a2 ){
    	return ! estPlusPetit( a2 );
    }
}