import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;

//interface : definir des types statiques.
//pas d'instance, pas de constructeur.
//--> Qualification pour les autres types.
//-> representee par un (des) contrat(s).
//  contrat : signature de methode.
//
//declaration : methode (public, abstract)
//champ (public, static, final)
//avantage : implementer plusieurs interfaces.
//independant de l'arbre d'heritage.
public class Principal {
	public static <T> void p( Iterable<T> t ) {
		Iterator< T > i = t.iterator();
		while( i.hasNext() ) {
			T x = i.next();

			System.out.println( x );
		}
	}
	public static double sommeAire( Forme2D [] t ) {
		double somme = 0.0;

		for( int i = 0; i < t.length; ++ i ) {
			somme += t[i].aire();
		}

		return somme;
	}


	public static void main( String [] args ) {
		Forme2D [] f  = { new Cercle( 3.0 ),
				new Rectangle( 2.3, 4.23 ),
				new Cercle( 1.2 )
		};

		sommeAire( f );
		Cercle c1 = new Cercle( 3.4 );
		Cercle c2 = new Cercle( 5.4 );

		if( c1.estPlusGrand( c2 ) ) {
			System.out.println( "c1 est plus grand que c2" );
		} else {
			System.out.println("c2 est plus grand que c1");
		}

		int [] tab = {1,2,3};
		for( int i = 0; i < tab.length; ++ i ) {
			int x = tab[i];

			System.out.println( x );
		}

		ArrayList< Integer > a = new ArrayList<>();
		for( int j = 0; j < a.size(); ++ j ) {
			Integer y = a.get(j);

			System.out.println( y );
		}
		// p :: position
		// init :: valeur initiale pour la position
		// test :: tester la fin de la boucle
		// suivant :: aller a la prochaine position.
		// courant :: lire l'element courant.
		//
		// Structure s;
		// for( p = init; p.test(); p.suivant() ) {
		//  	var v = s.courant( p );
		//}

		// en java :
		// Structure(Classe) :: Iterable
		// Iterable -> construire un Iterator
		// Iterator -> next(), hasNext()
		Iterable<Integer> t = new ArrayList<Integer>();
		Iterator<Integer> it = t.iterator();
		while( it.hasNext() ) {
			Integer x = it.next();
		}

		Vector< Integer > c = new Vector<>();

		c.add( 2 );
		c.add( 3 );
		c.add( 6 );

		Iterator< Integer > it2 = c.iterator();
		while( it2.hasNext() ) {
			Integer z = it2.next();

			System.out.println( z );
		}

		for( Integer x : c ) {
			System.out.println( x );
		}

	}
}