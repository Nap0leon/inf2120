public class Animal {

}

public class Oiseau extends Animal {
    public void seDeplacer() {
        System.out.println("Je vole");
    }
}

public class Chien extends Animal {
    public void seDeplacer() {
        System.out.println("Je cours");
    }
}

public static void affichierDeplacement(Animal [] lesAnimaux) {
    for (Animal a : lesAnimaux) {
        // test si a est une instance de oiseau
        if (a instanceof Oiseau) {
            ((Oiseau)a).seDeplacer();   // Besoin d'un cast
        // test si a est une instance de oiseau
        } else if (a instanceof Chien) {
            ((Chien)a).seDeplacer();    // Besoin d'un cast
        }
    }
}

public static void affichierDeplacement(Animal[] lesAnimaux) {
    for (Animal a : lesAnimaux) {
        a.seDeplacer(); // compile sans conversion de type
    }
}