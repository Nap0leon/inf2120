public class Cavalier extends PieceEchec {
	public Cavalier( Echiquier echiquier, int ligne, Colonne colonne, Couleur couleur) {
		super( echiquier, ligne, colonne, couleur );
	}
	
	@Override
	public boolean deplacementValide(int ligne, Colonne colonne) {
		int deltaColonne = Math.abs( this.colonne.ordinal() - colonne.ordinal() );
		int deltaLigne = Math.abs( this.ligne - ligne );

		return ( deltaColonne == 1 && deltaLigne == 2 ) || ( deltaColonne == 2 && deltaLigne == 1 );
	}
	
	@Override
	public String chaineReduite() {
		return "C" + couleur.chaineReduite();
	}

	@Override
	public String toString() {
		return "Cavalier" + super.toString();
	}
}