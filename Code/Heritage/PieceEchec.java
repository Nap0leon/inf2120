public abstract class PieceEchec {
	protected Echiquier echiquier;
	protected int ligne;
	protected Colonne colonne;
	protected Couleur couleur;

	/**
	 * Construit une nouvelle piece d'echec a la position indique.
	 * @param ligne Numero de la ligne pour la nouvelle piece.  Cette valeur doit
	 * etre entre 1 et 8 inclusivement.
	 * @param colonne Indique la colonne pour la nouvelle piece.
	 * @param couleur Indique la couleur de la nouvelle piece.
	 */
	public PieceEchec( Echiquier echiquier, int ligne, Colonne colonne, Couleur couleur ) {
		this.echiquier = echiquier;
		this.ligne = ligne;
		this.colonne = colonne;
		this.couleur = couleur;
	}

	
	public abstract boolean deplacementValide( int ligne, Colonne colonne );
	

	public void deplacer( int ligne, Colonne colonne ) throws DeplacementInvalide {
		if( deplacementValide( ligne, colonne ) ) {
			this.ligne = ligne;
			this.colonne = colonne;
		} else {
			throw new DeplacementInvalide();
		}
	}

	public String chaineReduite() {
		return "??";
	}
	
	@Override
	public String toString() {
		return "( " +
	           ligne +
	           ", " +
	           colonne +
	           ", " +
	           couleur +
	           " )";
	}
}