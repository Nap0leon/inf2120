public class Roi extends PieceEchec {
	public Roi( Echiquier echiquier, int ligne, Colonne colonne, Couleur couleur) {
		super( echiquier, ligne, colonne, couleur );
	}	

	@Override
	public boolean deplacementValide(int ligne, Colonne colonne) {
		int deltaColonne = Math.abs( this.colonne.ordinal() - colonne.ordinal() );
		int deltaLigne = Math.abs( this.ligne - ligne );

		return ( deltaColonne == 1 && deltaLigne == 1 )
			   ||
			   ( deltaColonne == 1 && deltaLigne == 0 )
			   ||
			   ( deltaColonne == 0 && deltaLigne == 1 );
	}

	@Override
	public String chaineReduite() {
		return "R" + couleur.chaineReduite();
	}

	@Override
	public String toString() {
		return "Roi" + super.toString();
	}
}