public class Dame extends PieceEchec {
	public Dame( Echiquier echiquier, int ligne, Colonne colonne, Couleur couleur) {
		super( echiquier, ligne, colonne, couleur );
	}	

	@Override
	public boolean deplacementValide(int ligne, Colonne colonne) {
		int deltaColonne = Math.abs( this.colonne.ordinal() - colonne.ordinal() );
		int deltaLigne = Math.abs( this.ligne - ligne );

		return ( ( deltaColonne != 0 ) && ( deltaColonne == deltaLigne ) )
				||
			   ( ( this.ligne == ligne ) ^ ( this.colonne == colonne ) );
	}
	
	@Override
	public String chaineReduite() {
		return "D" + couleur.chaineReduite();
	}

	@Override
	public String toString() {
		return "Dame" + super.toString();
	}
}