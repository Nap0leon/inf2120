public class Echiquier {
	public static final int NOMBRE_LIGNE = 8;
	public static final int NOMBRE_COLONNE = 8;
	
	public static final int LIGNE_PIECE_BLANCHE = 1;
	public static final int LIGNE_PIECE_NOIRE = 8;
	public static final int LIGNE_PION_BLANC = 2;
	public static final int LIGNE_PION_NOIR = 7;
	
	public static final Colonne COLONNE_TOUR_DAME = Colonne.A;
	public static final Colonne COLONNE_CAVALIER_DAME = Colonne.B;
	public static final Colonne COLONNE_FOU_DAME = Colonne.C;
	public static final Colonne COLONNE_DAME = Colonne.D;
	public static final Colonne COLONNE_ROI = Colonne.E;
	public static final Colonne COLONNE_FOU_ROI = Colonne.F;
	public static final Colonne COLONNE_CAVALIER_ROI = Colonne.G;
	public static final Colonne COLONNE_TOUR_ROI = Colonne.H;
	
	protected  PieceEchec [][] plateau = 
		new  PieceEchec [NOMBRE_LIGNE][NOMBRE_COLONNE];

	
	/**
	 * ajoute une piece sur l'echiquier.  Si la case de la piece
	 * est deja occupe, l'ancienne piece est enleve du plateau.
	 * @param piece La piece a ajouter.  La piece sera ajoute sur le plateau a la
	 * position enregistre dans la piece.
	 */
	private void ajouterPiece( PieceEchec piece ) {
		plateau[ piece.ligne - 1 ][ piece.colonne.ordinal() ] = piece;
	}
	
	/**
	 * Constructeur vide.
	 */
	public Echiquier() {
	}
	
	/**
	 * Positionne le plateau pour le debut d'une partie.
	 */
	public void initialiserPlateau() {
		for( Colonne c : Colonne.COLONNES ) {
			ajouterPiece( new Pion( this, LIGNE_PION_BLANC, c, Couleur.BLANC ) );
			ajouterPiece( new Pion( this, LIGNE_PION_NOIR,  c, Couleur.NOIR ) );
		}
		
		ajouterPiece( new Tour    ( this, LIGNE_PIECE_BLANCHE, COLONNE_TOUR_DAME,     Couleur.BLANC ) );
		ajouterPiece( new Cavalier( this, LIGNE_PIECE_BLANCHE, COLONNE_CAVALIER_DAME, Couleur.BLANC ) );
		ajouterPiece( new Fou     ( this, LIGNE_PIECE_BLANCHE, COLONNE_FOU_DAME,      Couleur.BLANC ) );
		ajouterPiece( new Dame    ( this, LIGNE_PIECE_BLANCHE, COLONNE_DAME,          Couleur.BLANC ) );
		ajouterPiece( new Roi     ( this, LIGNE_PIECE_BLANCHE, COLONNE_ROI,           Couleur.BLANC ) );
		ajouterPiece( new Fou     ( this, LIGNE_PIECE_BLANCHE, COLONNE_FOU_ROI,       Couleur.BLANC ) );
		ajouterPiece( new Cavalier( this, LIGNE_PIECE_BLANCHE, COLONNE_CAVALIER_ROI,  Couleur.BLANC ) );
		ajouterPiece( new Tour    ( this, LIGNE_PIECE_BLANCHE, COLONNE_TOUR_ROI,      Couleur.BLANC ) );

		ajouterPiece( new Tour    ( this, LIGNE_PIECE_NOIRE,   COLONNE_TOUR_DAME,     Couleur.NOIR ) );
		ajouterPiece( new Cavalier( this, LIGNE_PIECE_NOIRE,   COLONNE_CAVALIER_DAME, Couleur.NOIR ) );
		ajouterPiece( new Fou     ( this, LIGNE_PIECE_NOIRE,   COLONNE_FOU_DAME,      Couleur.NOIR ) );
		ajouterPiece( new Dame    ( this, LIGNE_PIECE_NOIRE,   COLONNE_DAME,          Couleur.NOIR ) );
		ajouterPiece( new Roi     ( this, LIGNE_PIECE_NOIRE,   COLONNE_ROI,           Couleur.NOIR ) );
		ajouterPiece( new Fou     ( this, LIGNE_PIECE_NOIRE,   COLONNE_FOU_ROI,       Couleur.NOIR ) );
		ajouterPiece( new Cavalier( this, LIGNE_PIECE_NOIRE,   COLONNE_CAVALIER_ROI,  Couleur.NOIR ) );
		ajouterPiece( new Tour    ( this, LIGNE_PIECE_NOIRE,   COLONNE_TOUR_ROI,      Couleur.NOIR ) );
		
		for( int i = LIGNE_PION_BLANC; i < LIGNE_PION_NOIR - 1; ++ i ) {
			for( int j = 0; j < NOMBRE_COLONNE; ++ j ) {
				plateau[i][j] = null;
			}
		}		
	}

	@Override
	public String toString() {
		String resultat = "   a  b  c  d  e  f  g  h\n";
		
		for( int i = NOMBRE_LIGNE - 1; i >= 0; -- i ) {
			resultat += ( i + 1 ) + " ";
			for( int j = 0; j < NOMBRE_COLONNE; ++ j ) {
				if( plateau[i][j] == null ) {
					resultat += ( ( i + j ) % 2 == 0 ) ? ".. " : "   ";
				} else {
				    resultat += plateau[i][j].chaineReduite() + " ";
				}
			}
			if( i % 2 == 0 ) {
				resultat += "\n  ..    ..    ..    ..\n";
			} else {
				resultat += "\n     ..    ..    ..    ..\n";
			}
		}
		
		return resultat;
	}
}