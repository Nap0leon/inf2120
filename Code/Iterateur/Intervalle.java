import java.util.Iterator;

//------------------------------------------------------------------------------
/**
 * Permet de construire une abstraction pour iterer sur des intervalles
 * numeriques.  Exemple d'utilisation :
 * <pre>{@code
 * for( Integer x : new Intervalle( 6 ) ) {
 *     System.out.print( x + " " );
 * }
 * System.out.println( "" );
 * }</pre>
 * va afficher le résultat : 0 1 2 3 4 5 6
 * <pre>{@code
 * for( Integer y : new Intervalle( 3, 8 ) ) {
 *     System.out.print( y + " " );
 * }
 * System.out.println( "" );
 * }</pre>
 * va afficher le résultat : 3 4 5 6 7 8
 * <pre>{@code
 * for( Integer z : new Intervalle( 10, 50, 5 ) ) {
 *     System.out.print( z + " " );
 * }
 * System.out.println( "" );
 * }</pre>
 * va afficher le résultat : 10 15 20 25 30 35 40 45 50.
 * @author Bruno Malenfant
 */
public class Intervalle implements Iterable< Integer > {
	
	/**
	 * Indique la première valeur qui l'itérateur prendra.
	 * Par défaut, cette valeur est {@value}.
	 */
	private int _debut = 0;
	
	/**
	 * Indique la plus grande valeur possible que peu prendre 
	 * l'itérateur.
	 */
	private int _fin;
	
	/**
	 * Indique l'incrément qui sera donnée à chaque itération.
	 * Par défaut, cette valeur est {@value}.
	 */
	private int _pas = 1;
	
	/**
	 * Constructeur permettant de définir la valeur maximum que l'itérateur peut
	 * prendre.  Cela va produire le même résultat que la construction :
	 * {@code Intervalle( 0, a_fin, 1 )}.
	 * @param a_fin Un entier indiquant la plus grande valeur possible
	 *              pour l'itérateur.
	 *              Si cette valeur est plus petite que {@code 0}, alors il n'y aura pas d'itération.
	 */
	public Intervalle( int a_fin ) {
		_fin = a_fin;
	}
	
	/**
	 * Constructeur permettant de définir la première et dernière valeur que prendra
	 * un itérateur.  Cela va produire le même résultat que la construction :
	 * {@code Intervalle( a_debut, a_fin, 1 )}.
	 * @param a_debut Un entier indiquant la première valeur que prendra l'itérateur.
	 * @param a_fin Un entier indiquant la dernière valeur que prendra l'itérateur.
	 *              Si cette valeur est plus petite que {@code a_debut}, alors il n'y aura pas d'itération.
	 */
	public Intervalle( int a_debut, int a_fin ) {
		_debut = a_debut;
		_fin = a_fin;
	}
	
	/**
	 * Constructeur permettant de définir un intervalle de valeur.
	 * @param a_debut Un entier indiquant la première valeur que prendra l'itérateur.
	 * @param a_fin Un entier qui indique la valeur maximum que peut prendre l'itérateur.
	 *              Cette valeur ne sera pas nécessairement utilisé, dependant du {@code pas} spécifié.
	 *              Si cette valeur est plus petite que {@code a_debut}, alors il n'y aura pas d'itération.
	 * @param a_pas Un entier qui indique l'incrément appliqué à chaque changement de valeur.
	 */
	public Intervalle( int a_debut, int a_fin, int a_pas ) {
		_debut = a_debut;
		_fin = a_fin;
		_pas = a_pas;
	}
	
	/**
	 * Retourne un itérateur sur l'intervalle construit.
	 * @return un itérateur sur l'intervalle construit.
	 */
	@Override
	public Iterator< Integer > iterator() {
		return new IterateurIntervalle( this );
	}
	
	private class IterateurIntervalle implements Iterator< Integer > {
		private Intervalle _sujet;
		private int _courrant;
		
		public IterateurIntervalle( Intervalle a_sujet ) {
			_sujet = a_sujet;
			_courrant = _sujet._debut;
		}
		
		@Override
		public boolean hasNext() {
			return _courrant <= _sujet._fin;
		}
		
		@Override
		public Integer next() {
			int temp = _courrant;
			
			_courrant += _sujet._pas;
			
			return temp;
		}
	}
}