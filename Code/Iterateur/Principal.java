public class Principal {
	public static void main( String argv[] ) {
		for( Integer x : new Intervalle( 6 ) ) {
			System.out.print( x + " " );
		}
		System.out.println( "" );

		for( Integer y : new Intervalle( 3, 8 ) ) {
			System.out.print( y + " " );
		}
		System.out.println( "" );
		
		for( Integer z : new Intervalle( 10, 50, 5 ) ) {
			System.out.print( z + " " );
		}
		System.out.println( "" );
	}
}