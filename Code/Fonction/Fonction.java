public interface Fonction<TypeParametre, TypeRetour> {
	TypeRetour application( TypeParametre x );
}