public class F implements Fonction<Integer, Integer> {
    @Override
    public Integer appliquer( Integer x) {
        return 2 * x;
    }
}