import com.sun.org.apache.xpath.internal.functions.Function;

public class Principal {
	public static void main( String [] args ) {
		Supplier< Integer > s = new Quatre();
		
		System.out.println( s.get() );
		System.out.println( s.get() );
		System.out.println( s.get() );
		System.out.println( s.get() );
		
		Predicate<Integer> p = new EstPair();
		
		System.out.println( p.test( 5 ) );
		System.out.println( p.test( 8 ) );
		
		Somme c = new Somme();
		
		c.accept( 3 );
		c.accept( 7 );
		c.accept( 2 );
		c.accept( 8 );
		
		System.out.println( c.lireS() );
		
		// Classe instantanee (anonyme).
		// syntaxe.
		Function< Integer, Integer > f =
			new Function<Integer, Integer>(){
			public Integer apply( Integer x ) {
				return 2 * x + 1;
			}			
		};
		
		System.out.println( f.apply( 4 ) );
		System.out.println( f.apply( 7 ) );
		System.out.println( f.apply( 2 ) );
		
		// java 8 : Lambda
		
		Function<Integer, Integer> g = 
				( x ) -> { return 3 * x + 1;  };
				
		System.out.println( g.apply( 4 ) );
				
		Function< Integer, Integer > s2 = 
				x -> {
					int y = 0;
					for( int i = 0; i <= x; ++ i ) {
						y += i;
					}
					return y;
				};
		System.out.println( s2.apply( 10 ) );
	}
}