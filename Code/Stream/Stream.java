import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class Stream {

    public static void main(String[] args) {
        List<Integer> a = Arrays.asList(3, 5, 6, 7, 8, 4, 9, 2, 4, 6, 54, 2, 5, 7);

        Stream< Integer > s1 = a.stream();
        Stream< Integer > s2 = s1.map (x -> 2* x);
        Stream< Integer > s3 = s2.filter(x -> x < 10);
        Stream< Integer > s4 = s3.map(x -> x - 1);

        s4.forEach(System.out::println);
        // c'est la ligne precedente qui part la machine.

        // equivalent de toutes les lignes ci-dessus :
        a.stream()
                .map (x -> 2* x)
                .filter(x -> x < 10)
                .map(x -> x - 1)
                .forEach(System.out::println);

        // parallelisation :
        a.stream().parallel()
                .map (x -> 2* x)
                .filter(x -> x < 10)
                .map(x -> x - 1)
                .forEach(System.out::println);

        // System.out::println ====  x -> System.out.println(x)
        // ATTENTION foreach est terminal : le stream est detruit apres sa
        // consommation
    }
}