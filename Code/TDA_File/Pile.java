import java.util.EmptyStackException;

public interface Pile<String> {
    public abstract boolean estVide();

    public abstract void empiler(String element);

    public abstract String depiler();

    public abstract String sommet();

    public abstract void vider();

    public abstract int taille();

}