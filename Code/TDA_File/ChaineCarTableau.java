import java.util.NoSuchElementException;

public class ChaineCarTableau implements TDAChaineCar {
    char[] tab;
    public ChaineCarTableau(char[] tab) {
        this.tab = tab;
    }

    @Override
    public int longueur() {
        return tab.length;
    }

    @Override
    public char iEme(int i) throws IndexOutOfBoundsException {
        if (i >= 0 && i < tab.length)
            return tab[i];
        else
            throw new IndexOutOfBoundsException();
    }

    @Override 
    public int indiceDe(char c) throws NoSuchElementException {
        int i = 0;
        while (i < tab.length && (tab[i] != c)) {
            i++;
        }
        if (i < tab.length) {
            return i;
        } else {
            throw new NoSuchElementException();
        }

    }

}